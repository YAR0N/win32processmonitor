#include <iostream>
#include "ProcessMonitor.h"

using namespace std;

void startCallback() {
	cout << "Start" << endl;
}

void stopCallback() {
	cout << "Stop" << endl;
}

void manualStopCallback() {
	cout << "ManualStop" << endl;
}

int main() {
	function<void()> start = startCallback;
	function<void()> stop = stopCallback;
	function<void()> mStop = manualStopCallback;

	HANDLE th;
	//ProcessMonitor ph(th, 3332);
	ProcessMonitor ph(th, "C:\\Users\\yaron\\Desktop\\putty.exe", "-ssh");
	ph.onProcStart(start);
	ph.onProcStop(stop);
	ph.onProcManuallyStopped(mStop);
	Sleep(1000);
	cout << ph.getStatus() << endl;
	ph.stop();
	Sleep(1000);
	cout << ph.getStatus() << endl;
	ph.start();
	Sleep(1000);
	cout << ph.getStatus() << endl;

	WaitForSingleObject(th, INFINITE);
}

