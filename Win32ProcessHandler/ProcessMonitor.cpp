#include "ProcessMonitor.h"
#include <regex>
#include <Wbemidl.h>
#include <atlbase.h>
#include <iostream>
#include <atlstr.h>

#pragma comment(lib, "wbemuuid.lib")

const int ProcessMonitor::STATUS_STOPPED = 0;
const int ProcessMonitor::STATUS_WORKING = 1;

ProcessMonitor::ProcessMonitor(HANDLE &th, LoggerInterface& logger, const std::string& app, const std::string& commandL) :
_app(app),
_commandL(commandL),
monitorH(th),
_status(STATUS_STOPPED),
onProcStartF(0),
onProcStopF(0),
onProcManuallyStoppedF(0),
_logger(logger)
{
	InitializeCriticalSection(&ssSection);
	start();
}

ProcessMonitor::ProcessMonitor(HANDLE &th, LoggerInterface& logger, DWORD processId) :
_app(),
_commandL(),
processId(processId),
monitorH(th),
_status(STATUS_STOPPED),
onProcStartF(0),
onProcStopF(0),
onProcManuallyStoppedF(0),
_logger(logger)
{
	InitializeCriticalSection(&ssSection);
	processH = OpenProcess(
		PROCESS_ALL_ACCESS,
		FALSE,
		processId);
	if (processH != NULL && getArguments()){
		_status = STATUS_WORKING;
		monitor();
	}
	else {
		throw new std::invalid_argument("This process can not be monitored");
	}
}

ProcessMonitor::~ProcessMonitor()
{
	stop();
	DeleteCriticalSection(&ssSection);
}

void ProcessMonitor::start()
{
	EnterCriticalSection(&ssSection);
	if (_status == STATUS_WORKING) 
		return;

	_terminateFlag = true;
	if (launch())
		monitor();
	LeaveCriticalSection(&ssSection);
}

UINT ProcessMonitor::stop()
{
	EnterCriticalSection(&ssSection);
	UINT exitCode = 0;

	if (_status == STATUS_STOPPED)
		return exitCode;

	_terminateFlag = false;
	if (TerminateProcess(processH, exitCode)) {
		_status = STATUS_STOPPED;
		if (onProcManuallyStoppedF != 0)
			(*onProcManuallyStoppedF)();
		_logger.logMStop();
	}
	close();
	LeaveCriticalSection(&ssSection);
	return exitCode;
}

void ProcessMonitor::monitor()
{
	monitorH = CreateThread(
		NULL,
		0,
		ProcessMonitor::monitorThread,
		this,
		0,
		&monitorId);
}

void ProcessMonitor::close()
{
	CloseHandle(processH);
	CloseHandle(processThreadH);
	CloseHandle(monitorH);
}

bool ProcessMonitor::launch()
{
	STARTUPINFO si;
	PROCESS_INFORMATION pi;

	ZeroMemory(&si, sizeof(si));
	ZeroMemory(&pi, sizeof(pi));

	LPSTR cl = const_cast<LPSTR>(_commandL.data());
	if (CreateProcess(_app.data(), cl, NULL, NULL, FALSE, 0, NULL, NULL, &si, &pi)) {
		_status = STATUS_WORKING;

		processId = pi.dwProcessId;
		processH = pi.hProcess;
		processThreadH = pi.hThread;

		if (onProcStartF != 0)
			(*onProcStartF)();
		_logger.logStart();
		return true;
	}
	else {
		return false;
	}
}

bool ProcessMonitor::getArguments() {
	HRESULT hres;

	hres = CoInitializeEx(0, COINIT_MULTITHREADED);
	if (FAILED(hres)) return false;

	hres = CoInitializeSecurity(
		NULL,
		-1,                          
		NULL,                        
		NULL,                       
		RPC_C_AUTHN_LEVEL_DEFAULT,   
		RPC_C_IMP_LEVEL_IMPERSONATE, 
		NULL,                        
		EOAC_NONE,                  
		NULL                        
		);
	if (FAILED(hres)) {
		CoUninitialize();
		return false;
	}

	IWbemLocator *pLoc = NULL;
	hres = CoCreateInstance(
		CLSID_WbemLocator,
		0,
		CLSCTX_INPROC_SERVER,
		IID_IWbemLocator, (LPVOID *)&pLoc);
	if (FAILED(hres)) {
		CoUninitialize();
		return false;
	}

	IWbemServices *pSvc = NULL;
	hres = pLoc->ConnectServer(
		L"ROOT\\CIMV2",
		NULL,                   
		NULL,                    
		0,                      
		NULL,                   
		0,                      
		0,                       
		&pSvc                   
		);
	if (FAILED(hres)) {
		pLoc->Release();
		CoUninitialize();
		return false;
	}

	hres = CoSetProxyBlanket(
		pSvc,                       
		RPC_C_AUTHN_WINNT,          
		RPC_C_AUTHZ_NONE,           
		NULL,                       
		RPC_C_AUTHN_LEVEL_CALL,      
		RPC_C_IMP_LEVEL_IMPERSONATE, 
		NULL,                        
		EOAC_NONE                   
		);
	if (FAILED(hres))
	{
		pSvc->Release();
		pLoc->Release();
		CoUninitialize();
		return false;              
	}

	char cId[20];
	_ultoa_s(processId, cId, 10);
	CComBSTR query("SELECT * FROM Win32_Process WHERE ProcessId = ");
	query.Append(cId);
	IEnumWbemClassObject* pEnumerator = NULL;
	hres = pSvc->ExecQuery(
		L"WQL",
		query,
		WBEM_FLAG_FORWARD_ONLY | WBEM_FLAG_RETURN_IMMEDIATELY,
		NULL,
		&pEnumerator);
	if (FAILED(hres))
	{
		pSvc->Release();
		pLoc->Release();
		CoUninitialize();
		return false;               
	}

	IWbemClassObject *result = NULL;
	ULONG uReturn = 0;
	if(pEnumerator)
	{
		HRESULT hr = pEnumerator->Next(WBEM_INFINITE, 1,
			&result, &uReturn);

		if (0 == uReturn)
		{
			return false;
		}

		VARIANT commandL;
		VARIANT path;

		hr = result->Get(L"ExecutablePath", 0, &path, 0, 0);
		hr = result->Get(L"CommandLine", 0, &commandL, 0, 0);

		std::wstring wp(path.bstrVal);
		std::string sp(wp.begin(), wp.end());
		_app = sp;

		std::wstring wc(commandL.bstrVal);
		std::string sc(wc.begin(), wc.end());
		std::regex argsR("^(?:\\\".+\\\"|[^\\s\"]+)\\s(.+)$");
		std::smatch match;
		if (std::regex_match(sc, match, argsR) && match.size() == 2){
			_commandL = match[1].str();
		}

		VariantClear(&commandL);
		VariantClear(&path);

		result->Release();
	}

	pSvc->Release();
	pLoc->Release();
	pEnumerator->Release();
	CoUninitialize();
	return true;
}

DWORD WINAPI ProcessMonitor::monitorThread(LPVOID lpParam)
{
	ProcessMonitor* ph = (ProcessMonitor*)lpParam;
	WaitForSingleObject(ph->processH, INFINITE);
	while (ph->_terminateFlag) {
		ph->_logger.logStop();
		if (ph->launch()){
			WaitForSingleObject(ph->processH, INFINITE);
			ph->_status = STATUS_STOPPED;
			if (ph->onProcStopF != 0 && ph->_terminateFlag) {
				(*ph->onProcStopF)();
			}
		}
		else{
			return 1;
		}
	}
	return 0;
}

LoggerEvent::LoggerEvent()
{
	eventLogH = RegisterEventSource(NULL, "ProcessMonitor");
}

LoggerEvent::~LoggerEvent()
{
	if (eventLogH)
		DeregisterEventSource(eventLogH);
}

void LoggerEvent::log(DWORD ms) const
{
	if (eventLogH != NULL)
		ReportEvent(eventLogH, EVENTLOG_INFORMATION_TYPE, NULL, ms, NULL, 0, 0, NULL, NULL);
}

LoggerFile::LoggerFile(LPCSTR fileName)
{
	fileH = CreateFile(
		fileName,
		GENERIC_WRITE,
		0,
		NULL,
		CREATE_ALWAYS,
		FILE_ATTRIBUTE_NORMAL,
		NULL);
	if (fileH == INVALID_HANDLE_VALUE)
		throw new std::runtime_error("Can not open file");
}

LoggerFile::~LoggerFile()
{
	CloseHandle(fileH);
}

DWORD LoggerFile::log(const char* data) const
{
	SYSTEMTIME st;
	GetLocalTime(&st);
	CString tw;

	tw.Format("[%d-%02d-%02d %02d:%02d:%02d.%03d] ",
		st.wYear,
		st.wMonth,
		st.wDay,
		st.wHour,
		st.wMinute,
		st.wSecond,
		st.wMilliseconds);

	tw.Append(data);

	DWORD sizeW = 0;
	WriteFile(
		fileH,
		tw.GetBuffer(),
		tw.GetLength(),
		&sizeW,
		NULL);
	return sizeW;
}

const char* LoggerFile::startM = "Process started \r\n";
const char* LoggerFile::stopM = "Process stoped \r\n";
const char* LoggerFile::stopMM = "Process manually stoped \r\n";