MessageIdTypedef=DWORD

SeverityNames=(Informational=0x0:STATUS_SEVERITY_INFORMATIONAL)

FacilityNames=(System=0x0:FACILITY_SYSTEM)

LanguageNames=(English=0x409:MSG00409)

MessageId=0x1
Severity=Informational
Facility=System
SymbolicName=MSG_START_PROCESS
Language=English
Process started
.

MessageId=0x2
Severity=Informational
Facility=System
SymbolicName=MSG_STOP_PROCESS
Language=English
Process stoped
.

MessageId=0x3
Severity=Informational
Facility=System
SymbolicName=MSG_MANUAL_STOP_PROCESS
Language=English
Process manualy stoped
.
