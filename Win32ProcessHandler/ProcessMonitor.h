#pragma once
#include <windows.h>
#include <string>
#include <functional>
#include <mutex>
#include "Messages.h"

class LoggerInterface;

class ProcessMonitor
{
public:
	ProcessMonitor(HANDLE &th, LoggerInterface& logger, const std::string& app, const std::string& commandL = "");
	ProcessMonitor(HANDLE &th, LoggerInterface& logger, DWORD processId);
	~ProcessMonitor();

	int getStatus() { std::lock_guard<std::mutex> g(getMutex); return _status; }
	HANDLE getHandle() { std::lock_guard<std::mutex> g(getMutex); return processH; }
	DWORD getId() { std::lock_guard<std::mutex> g(getMutex); return processId; }
	
	void start();
	UINT stop();

	void onProcStart(const std::function<void()>& fn) { std::lock_guard<std::mutex> g(setMutex); onProcStartF = &fn; }
	void onProcStop(const std::function<void()>& fn) { std::lock_guard<std::mutex> g(setMutex); onProcStopF = &fn; }
	void onProcManuallyStopped(const std::function<void()>& fn) { std::lock_guard<std::mutex> g(setMutex); onProcManuallyStoppedF = &fn; }

private:

	int _status;
	bool _terminateFlag;

	std::string _app;
	std::string _commandL;

	DWORD processId;
	HANDLE processH;
	HANDLE processThreadH;

	HANDLE &monitorH;
	DWORD monitorId;

	LoggerInterface& _logger;

	const std::function<void()>* onProcStartF;
	const std::function<void()>* onProcStopF;
	const std::function<void()>* onProcManuallyStoppedF;

	void close();
	bool launch();
	void monitor();

	bool getArguments();

	static DWORD WINAPI monitorThread(LPVOID lpParam);

	CRITICAL_SECTION ssSection;
	std::mutex getMutex;
	std::mutex setMutex;

	static const int STATUS_STOPPED;
	static const int STATUS_WORKING;

	ProcessMonitor(const ProcessMonitor&);
	ProcessMonitor& operator=(const ProcessMonitor&);
};

class LoggerInterface
{
	friend ProcessMonitor;
public:
	virtual ~LoggerInterface(){}
private:
	virtual void logStart() = 0;
	virtual void logStop() = 0;
	virtual void logMStop() = 0;
};

class LoggerEvent: public LoggerInterface
{
public:
	LoggerEvent();
	~LoggerEvent();

private:

	HANDLE eventLogH;

	void log(DWORD ms) const;
	virtual void logStart() { log(MSG_START_PROCESS); }
	virtual void logStop() { log(MSG_STOP_PROCESS); }
	virtual void logMStop() { log(MSG_MANUAL_STOP_PROCESS); }
};

class LoggerFile : public LoggerInterface
{
public:
	LoggerFile(LPCSTR fileName);
	~LoggerFile();

private:

	static const char* startM;
	static const char* stopM;
	static const char* stopMM;

	HANDLE fileH;
	DWORD log(const char* data) const;

	virtual void logStart() { log(startM); }
	virtual void logStop() { log(stopM); }
	virtual void logMStop() { log(stopMM); }
};
