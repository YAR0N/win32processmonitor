//
//  Values are 32 bit values laid out as follows:
//
//   3 3 2 2 2 2 2 2 2 2 2 2 1 1 1 1 1 1 1 1 1 1
//   1 0 9 8 7 6 5 4 3 2 1 0 9 8 7 6 5 4 3 2 1 0 9 8 7 6 5 4 3 2 1 0
//  +---+-+-+-----------------------+-------------------------------+
//  |Sev|C|R|     Facility          |               Code            |
//  +---+-+-+-----------------------+-------------------------------+
//
//  where
//
//      Sev - is the severity code
//
//          00 - Success
//          01 - Informational
//          10 - Warning
//          11 - Error
//
//      C - is the Customer code flag
//
//      R - is a reserved bit
//
//      Facility - is the facility code
//
//      Code - is the facility's status code
//
//
// Define the facility codes
//
#define FACILITY_SYSTEM                  0x0


//
// Define the severity codes
//
#define STATUS_SEVERITY_INFORMATIONAL    0x0


//
// MessageId: MSG_START_PROCESS
//
// MessageText:
//
// Process started
//
#define MSG_START_PROCESS                ((DWORD)0x00000001L)

//
// MessageId: MSG_STOP_PROCESS
//
// MessageText:
//
// Process stoped
//
#define MSG_STOP_PROCESS                 ((DWORD)0x00000002L)

//
// MessageId: MSG_MANUAL_STOP_PROCESS
//
// MessageText:
//
// Process manualy stoped
//
#define MSG_MANUAL_STOP_PROCESS          ((DWORD)0x00000003L)

